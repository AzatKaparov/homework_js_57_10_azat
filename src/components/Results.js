import React from 'react';
import { v4 as uuidv4 } from 'uuid';

const Results = ({ total, peopleCount, avg, method, peoples, tipsPercent, delivery } ) => {
    let indivPeople;
    if (peoples) {
        let del;
        if (!isNaN(delivery)) {
            del = Math.ceil(delivery / peoples.length);
        }
        indivPeople = peoples.map(item => {
            let payment = parseInt(item.payment);
            return (<p key={uuidv4()}>{item.name}: <b>{Math.ceil(payment += (payment * (tipsPercent / 100)) + del)}</b></p>);
        });
    }

    return (
        <div className="results">
            <p>Общая сумма заказа: <b>{total}</b></p>
            {method === "equally"
                ? <div>
                    <p>Количество человек: <b>{peopleCount}</b></p>
                    <p>Каждый платит по: <b>{avg}</b></p>
                </div>
                : null
            }
            {method === "individual"
                ? <div>
                    {indivPeople}
                </div>
                : null
            }
        </div>
    );
};

export default Results;
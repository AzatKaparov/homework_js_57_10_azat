import React from 'react';

const FormItem = ({ title, type, value, onFormItemChange, id, personName, personPayment, removePeople, nameHandler, paymentHandler }) => {
    switch (type) {
        case "form-item":
            return (
                <div className="form-item">
                    <p>{title}</p>
                    <input
                        onChange={onFormItemChange}
                        type="number"
                        id={id}
                        value={value}
                    />
                </div>
            );
        case "individual-item":
            return (
                <div className="individual-item">
                    <input
                        onChange={nameHandler}
                        id={id}
                        type="text"
                        name="person"
                        value={personName}
                        placeholder="Enter your name"
                    />
                    <input
                        onChange={paymentHandler}
                        id={id}
                        type="number"
                        name="persomSum"
                        value={personPayment}
                        placeholder="Enter a sum"
                    />
                    <button onClick={removePeople} className="remove-btn">Remove</button>
                </div>
            )
        default:
            return null;
    }
};

export default FormItem;
import React, {useState} from 'react';
import FormItem from "../components/FormItem";
import { v4 as uuidv4 } from 'uuid';
import Results from "../components/Results";

const OrderMaker = () => {
    const [tipsPercent, setTipsPercent] = useState(15);
    const [delivery, setDelivery] = useState(0);
    const [calculateType, setCalculateType] = useState(null);
    const [orderSum, setOrderSum] = useState(0);
    const [people, setPeople] = useState(0);
    const [indivPeople, setIndivPeople] = useState([]);
    const [results, setResults] = useState(null)

    const radioHandle = e => {
        setResults(null);
        switch (e.target.id) {
            case "equally":
                setCalculateType("equally");
                break;
            case "individual":
                setCalculateType("individual");
                break;
            default:
                break;
        }
    }

    const equalItemHandler = e => {
        switch (e.target.id) {
            case "orderSum":
                setOrderSum(e.target.value);
                break;
            case "people":
                setPeople(e.target.value);
                break;
            default:
                break;
        }
    }

    const formInpHandle = (e) => {
        switch (e.target.id) {
            case "delivery":
                setDelivery(e.target.value);
                break;
            case "tips":
                setTipsPercent(e.target.value);
                break;
            default:
                break;
        }
    };

    const personNameHandler = e => {
        setIndivPeople(indivPeople.map(person => {
            if (person.id === e.target.id) {
                return {
                    ...person,
                    name: e.target.value
                };
            }
            return person;
        }));
    };

    const personPaymentHandler = e => {
        setIndivPeople(indivPeople.map(person => {
            if (person.id === e.target.id) {
                return {
                    ...person,
                    payment: e.target.value
                };
            }
            return person;
        }));
    };

    const addIndivPeople = e => {
        e.preventDefault();
        setIndivPeople([
            ...indivPeople,
            {id: uuidv4(), name: "", payment: 0}
        ]);
    };

    const removeIndivPeople = id => {
        setIndivPeople(indivPeople.filter(person => person.id !== id));
    };

    const giveResults = e => {
        e.preventDefault();
        switch (calculateType) {
            case "equally":
                let total = 0;
                const order = parseInt(orderSum);
                total += order;
                total += (order * (parseInt(tipsPercent) / 100));
                if (parseInt(delivery)) {
                    total += parseInt(delivery);
                }
                const avg = Math.ceil(total / parseInt(people));
                setResults(<Results
                    method={calculateType}
                    total={total}
                    avg={avg}
                    peopleCount={people}
                />);
                break;
            case "individual":
                if (indivPeople.length !== 0) {
                    let total = 0;
                    indivPeople.forEach(item => {
                        total += parseInt(item.payment);
                    })
                    total += total * (parseInt(tipsPercent) / 100);
                    if (parseInt(delivery)) {
                        total += parseInt(delivery);
                    }
                    setResults(<Results
                        method={calculateType}
                        total={total}
                        peoples={indivPeople}
                        tipsPercent={parseInt(tipsPercent)}
                        delivery={parseInt(delivery)}
                    />);
                } else {
                    return null;
                }
                break;
            default:
                return null;
        }
    };

    return (
        <div className="main-content">
            <h4>Сумма заказа считается:</h4>
            <form action="" onSubmit={giveResults}>
                <input
                    onChange={radioHandle}
                    type="radio"
                    name="calculationType"
                    id="equally"
                    value="equally"
                />
                <label htmlFor="equally">Поровну между всеми участниками</label>
                <br/>
                <input
                    onChange={radioHandle}
                    type="radio"
                    name="calculationType"
                    id="individual"
                    value="individual"
                />
                <label htmlFor="individual">Индивидуально для каждого</label>
                {calculateType === "equally" &&
                    <div className={"equal-items"}>
                        <FormItem
                            onFormItemChange={equalItemHandler}
                            title={"Человек"}
                            value={people}
                            id={"people"}
                            type={"form-item"}
                        />
                        <FormItem
                            onFormItemChange={equalItemHandler}
                            title={"Сумма заказа"}
                            value={orderSum}
                            id={"orderSum"}
                            type={"form-item"}
                        />
                    </div>
                }
                {calculateType === "individual" &&
                    <div className="individual-items">
                        {indivPeople.map(item => {
                            return (
                                <FormItem
                                    type={"individual-item"}
                                    key={item.id}
                                    id={item.id}
                                    personName={item.name}
                                    personPayment={item.payment}
                                    removePeople={() => removeIndivPeople(item.id)}
                                    nameHandler={personNameHandler}
                                    paymentHandler={personPaymentHandler}
                                />
                            )
                        })}
                        <button onClick={addIndivPeople} className="add-person">Add more</button>
                    </div>
                }


                <FormItem
                    onFormItemChange={formInpHandle}
                    title={"Процент чаевых"}
                    value={tipsPercent}
                    id={"tips"}
                    type={"form-item"}
                />
                <FormItem
                    onFormItemChange={formInpHandle}
                    title={"Доставка"}
                    value={delivery}
                    id={"delivery"}
                    type={"form-item"}
                />
                <button type="submit" className="calculate-btn">Рассчитать</button>
            </form>
            {results}
        </div>
    );
};

export default OrderMaker;
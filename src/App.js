import "./App.css";
import OrderMaker from "./containers/OrderMaker";

function App() {
  return (
    <div className="container">
      <OrderMaker/>
    </div>
  );
}

export default App;
